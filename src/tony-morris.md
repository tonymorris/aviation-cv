Tony Morris
===========

----

>  Senior RA-Aus Instructor, Grade 3 Instructor

> <instructor@tmorris.net> • 0408711099 •
> Brisbane, Australia

----

Employment History
------------------

2024-*present*
:   **Senior RA-Aus Flight Instructor**
:   Gold Coast Sports Flying Club

2022-*present*
:   **Grade 3 Flight Instructor**
:   Flightscope Aviation

2021-*present*
:   **Senior RA-Aus Flight Instructor**
:   Flightscope Aviation

2020-2021
:   **RA-Aus Flight Instructor**
:   Flightscope Aviation

Qualifications & Endorsements
-----------------------------

2023
:   **Aerobatics Endorsement > 3000ft**
:   Flightscope Aviation

2022
:   **Grade 3 Flight Instructor Rating**
:   Flightscope Aviation

2022
:   **Design Feature Training Endorsement**
:   Flightscope Aviation

2022
:   **Commercial Pilot Licence (Aeroplane)**
:   Flightscope Aviation

2021
:   **Senior RA Flight Instructor Rating (3-axis)**
:   Flightscope Aviation

2021
:   **Tailwheel Design Feature Endorsement**
:   Flightscope Aviation

2021
:   **Aviation English Language Proficiency Assessor (AELP6 & GELP)**
:   Civil Aviation Safety Authority

2021
:   **Formation Endorsement**
:   Flightscope Aviation

2020
:   **Retractable undercarriage Design Feature Endorsement**
:   Flightscope Aviation

2020
:   **Night VFR Rating**
:   Flightscope Aviation

2019
:   **RA Flight Instructor Rating (3-axis)**
:   Flightscope Aviation

2019
:   **Manual Propellor Pitch Control (MPPC) endorsement**
:   Flightscope Aviation

2018
:   **Private Pilot Licence (Aeroplane)**
:   Pathfinder Aviation

2018
:   **Remote Pilot Licence RPA Multi-rotor 7kg**
:   Wicked Copters

2016
:   **Recreational Pilot Licence (Aeroplane)**
:   Flight One

Experience
----------

<!--
<div style="color:#808080; font-style:italic; font-size:0.4em; margin:0">
  Logbook last updated: <b>${PILOTLOGBOOK_COMMIT_TIME}</b>

  Logbook revision: <b>${PILOTLOGBOOK_CI_COMMIT_SHA}</b>
</div>
-->

* Total Aeronautical Hours: **${PILOTLOGBOOK_HOURS}**
* Total Dual Hours: **${PILOTLOGBOOK_HOURS_DUAL}**
* Total Command Hours: **${PILOTLOGBOOK_HOURS_INCOMMAND}**
* Total Hours RA Instruction: **${PILOTLOGBOOK_HOURS_RA_INSTRUCTING}**
* Total Hours GA Instruction: **${PILOTLOGBOOK_HOURS_GA_INSTRUCTING}**
* Total Night Hours: **${PILOTLOGBOOK_HOURS_NIGHT}**

* Aircraft Type Experience
  * Cessna 182
  * Cessna 172
  * Cessna 162
  * Cessna 152
  * Piper PA-28R
  * American Champion Citabria 7GCBC
  * American Champion Super Decathlon 8KCAB
  * Aquila A210
  * Jabiru J170
  * Eurofox 3K
  * Foxbat A22LS
  * Magnus Fusion 212
  * Vans Aircraft RV-6
  * Vans Aircraft RV-14
  * Sling 2

References
----------

*available upon request*

----

> <instructor@tmorris.net> • 0408711099 •
> Brisbane, Australia

> [https://aviation.cv.tmorris.net/](https://aviation.cv.tmorris.net/)

<div style="color:#808080; font-style:italic; font-size:0.4em; text-align:right">
  Last updated: <b>${COMMIT_TIME}</b>

  Revision: <b>${CI_COMMIT_SHA}</b>
</div>
