#!/bin/sh

set -e
set -x

script_dir=$(dirname "$0")
dist_dir=${script_dir}/../public
src_dir=${script_dir}/../src
etc_dir=${script_dir}/../etc
styles_dir=${script_dir}/../styles
style_tex=${styles_dir}/cv.tex
style_css=${styles_dir}/cv.css
src_basename=tony-morris
src_filename=${src_basename}.md

pilotLogbookCiParams() {
  pilotlogbook_ci_params_uri="https://pilotlogbook.online/ci_params"
  pilotlogbook_ci_params_dir="${dist_dir}/pilotlogbook.online/ci_params"
  mkdir -p "${pilotlogbook_ci_params_dir}"

  wget "${pilotlogbook_ci_params_uri}/CI_PAGES_DOMAIN" --output-document ${pilotlogbook_ci_params_dir}/CI_PAGES_DOMAIN
  wget "${pilotlogbook_ci_params_uri}/CI_PAGES_URL" --output-document ${pilotlogbook_ci_params_dir}/CI_PAGES_URL
  wget "${pilotlogbook_ci_params_uri}/CI_PROJECT_TITLE" --output-document ${pilotlogbook_ci_params_dir}/CI_PROJECT_TITLE
  wget "${pilotlogbook_ci_params_uri}/CI_PROJECT_URL" --output-document ${pilotlogbook_ci_params_dir}/CI_PROJECT_URL
  wget "${pilotlogbook_ci_params_uri}/COMMIT_TIME" --output-document ${pilotlogbook_ci_params_dir}/COMMIT_TIME
  wget "${pilotlogbook_ci_params_uri}/GITLAB_USER_NAME" --output-document ${pilotlogbook_ci_params_dir}/GITLAB_USER_NAME
  wget "${pilotlogbook_ci_params_uri}/GITLAB_USER_EMAIL" --output-document ${pilotlogbook_ci_params_dir}/GITLAB_USER_EMAIL
  wget "${pilotlogbook_ci_params_uri}/CI_COMMIT_SHA" --output-document ${pilotlogbook_ci_params_dir}/CI_COMMIT_SHA
  wget "${pilotlogbook_ci_params_uri}/CI_PROJECT_VISIBILITY" --output-document ${pilotlogbook_ci_params_dir}/CI_PROJECT_VISIBILITY

  PILOTLOGBOOK_CI_PAGES_DOMAIN=$(cat ${pilotlogbook_ci_params_dir}/CI_PAGES_DOMAIN)
  PILOTLOGBOOK_CI_PAGES_URL=$(cat ${pilotlogbook_ci_params_dir}/CI_PAGES_URL)
  PILOTLOGBOOK_CI_PROJECT_TITLE=$(cat ${pilotlogbook_ci_params_dir}/CI_PROJECT_TITLE)
  PILOTLOGBOOK_CI_PROJECT_URL=$(cat ${pilotlogbook_ci_params_dir}/CI_PROJECT_URL)
  PILOTLOGBOOK_COMMIT_TIME=$(cat ${pilotlogbook_ci_params_dir}/COMMIT_TIME)
  PILOTLOGBOOK_GITLAB_USER_NAME=$(cat ${pilotlogbook_ci_params_dir}/GITLAB_USER_NAME)
  PILOTLOGBOOK_GITLAB_USER_EMAIL=$(cat ${pilotlogbook_ci_params_dir}/GITLAB_USER_EMAIL)
  PILOTLOGBOOK_CI_COMMIT_SHA=$(cat ${pilotlogbook_ci_params_dir}/CI_COMMIT_SHA)
  PILOTLOGBOOK_CI_PROJECT_VISIBILITY=$(cat ${pilotlogbook_ci_params_dir}/CI_PROJECT_VISIBILITY)
}

pilotLogbookNumbers() {
  pilotlogbook_numbers_uri="https://pilotlogbook.online/numbers"
  pilotlogbook_numbers_dir="${dist_dir}/pilotlogbook.online/numbers"
  mkdir -p "${pilotlogbook_numbers_dir}"

  wget "${pilotlogbook_numbers_uri}/hours" --output-document ${pilotlogbook_numbers_dir}/hours
  wget "${pilotlogbook_numbers_uri}/hours-dual" --output-document ${pilotlogbook_numbers_dir}/hours-dual
  wget "${pilotlogbook_numbers_uri}/hours-incommand" --output-document ${pilotlogbook_numbers_dir}/hours-incommand
  wget "${pilotlogbook_numbers_uri}/hours-ra-instructing" --output-document ${pilotlogbook_numbers_dir}/hours-ra-instructing
  wget "${pilotlogbook_numbers_uri}/hours-ga-instructing" --output-document ${pilotlogbook_numbers_dir}/hours-ga-instructing
  wget "${pilotlogbook_numbers_uri}/hours-night" --output-document ${pilotlogbook_numbers_dir}/hours-night

  PILOTLOGBOOK_HOURS=$(cat ${pilotlogbook_numbers_dir}/hours)
  PILOTLOGBOOK_HOURS_DUAL=$(cat ${pilotlogbook_numbers_dir}/hours-dual)
  PILOTLOGBOOK_HOURS_INCOMMAND=$(cat ${pilotlogbook_numbers_dir}/hours-incommand)
  PILOTLOGBOOK_HOURS_RA_INSTRUCTING=$(cat ${pilotlogbook_numbers_dir}/hours-ra-instructing)
  PILOTLOGBOOK_HOURS_GA_INSTRUCTING=$(cat ${pilotlogbook_numbers_dir}/hours-ga-instructing)
  PILOTLOGBOOK_HOURS_NIGHT=$(cat ${pilotlogbook_numbers_dir}/hours-night)
}

substitute() {
 sed -e "s#\${PILOTLOGBOOK_CI_PAGES_DOMAIN}#${PILOTLOGBOOK_CI_PAGES_DOMAIN}#g" \
     -e "s#\${PILOTLOGBOOK_CI_PAGES_URL}#${PILOTLOGBOOK_CI_PAGES_URL}#g" \
     -e "s#\${PILOTLOGBOOK_CI_PROJECT_TITLE}#${PILOTLOGBOOK_CI_PROJECT_TITLE}#g" \
     -e "s#\${PILOTLOGBOOK_CI_PROJECT_URL}#${PILOTLOGBOOK_CI_PROJECT_URL}#g" \
     -e "s#\${PILOTLOGBOOK_COMMIT_TIME}#${PILOTLOGBOOK_COMMIT_TIME}#g" \
     -e "s#\${PILOTLOGBOOK_GITLAB_USER_NAME}#${PILOTLOGBOOK_GITLAB_USER_NAME}#g" \
     -e "s#\${PILOTLOGBOOK_GITLAB_USER_EMAIL}#${PILOTLOGBOOK_GITLAB_USER_EMAIL}#g" \
     -e "s#\${PILOTLOGBOOK_CI_COMMIT_SHA}#${PILOTLOGBOOK_CI_COMMIT_SHA}#g" \
     -e "s#\${PILOTLOGBOOK_CI_PROJECT_VISIBILITY}#${PILOTLOGBOOK_CI_PROJECT_VISIBILITY}#g" \
     -e "s#\${CI_PAGES_DOMAIN}#${CI_PAGES_DOMAIN}#g" \
     -e "s#\${CI_PAGES_URL}#${CI_PAGES_URL}#g" \
     -e "s#\${CI_PROJECT_TITLE}#${CI_PROJECT_TITLE}#g" \
     -e "s#\${CI_PROJECT_URL}#${CI_PROJECT_URL}#g" \
     -e "s#\${COMMIT_TIME}#${COMMIT_TIME}#g" \
     -e "s#\${GITLAB_USER_NAME}#${GITLAB_USER_NAME}#g" \
     -e "s#\${GITLAB_USER_EMAIL}#${GITLAB_USER_EMAIL}#g" \
     -e "s#\${CI_COMMIT_SHA}#${CI_COMMIT_SHA}#g" \
     -e "s#\${CI_PROJECT_VISIBILITY}#${CI_PROJECT_VISIBILITY}#g" \
     -e "s#\${PILOTLOGBOOK_HOURS}#${PILOTLOGBOOK_HOURS}#g" \
     -e "s#\${PILOTLOGBOOK_HOURS_DUAL}#${PILOTLOGBOOK_HOURS_DUAL}#g" \
     -e "s#\${PILOTLOGBOOK_HOURS_INCOMMAND}#${PILOTLOGBOOK_HOURS_INCOMMAND}#g" \
     -e "s#\${PILOTLOGBOOK_HOURS_RA_INSTRUCTING}#${PILOTLOGBOOK_HOURS_RA_INSTRUCTING}#g" \
     -e "s#\${PILOTLOGBOOK_HOURS_GA_INSTRUCTING}#${PILOTLOGBOOK_HOURS_GA_INSTRUCTING}#g" \
     -e "s#\${PILOTLOGBOOK_HOURS_NIGHT}#${PILOTLOGBOOK_HOURS_NIGHT}#g" \
     ${src_dir}/${src_filename} > ${dist_dir}/${src_filename}
}

buildPdf() {
  pandoc --standalone --template ${style_tex} --from markdown --to context --variable papersize=A4 --output ${dist_dir}/${src_basename}.tex ${dist_dir}/${src_filename}
  mtxrun --path=${dist_dir} --result=${src_basename}.pdf --script context tony-morris.tex
}

buildHtml() {
  pandoc --standalone --include-in-header ${style_css} --from markdown --to html --output ${dist_dir}/${src_basename}.html ${dist_dir}/${src_filename} --metadata pagetitle=${src_basename}
}

buildDocx() {
  pandoc --standalone ${dist_dir}/${src_filename} --output ${dist_dir}/${src_basename}.docx
}

buildRtf() {
  pandoc --standalone ${dist_dir}/${src_filename} --output ${dist_dir}/${src_basename}.rtf
}

buildAllFormats() {
  buildPdf
  buildHtml
  buildDocx
  buildRtf
}

mkdir -p ${dist_dir}
pilotLogbookCiParams
pilotLogbookNumbers
substitute
buildAllFormats
rsync -aH ${etc_dir}/ ${dist_dir}
